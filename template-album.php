        <?php if(isset($album)) : ?>
        <h1 style="background-image:url('<?php echo $album->thumbs[0]->src; ?>');"><span><?php echo $album->title; ?></span></h1>
        <ul class="gallery">
          <?php foreach($album->thumbs as $thumb) : ?>
          <li>
            <figure><img src="<?php echo $thumb->src; ?>" alt="<?php echo $thumb->caption; ?>"/></figure>
            <?php if($thumb->title) : ?><figcaption><?php echo $thumb->title; ?></figcaption><?php endif; ?>
          </li>
          <?php endforeach; ?>
        </ul>
        <?php endif; ?>
