<?php
include('common.php');

$album = null;
if(isset($_GET['id']))
  foreach($albums as $a)
    if($a->href == $_GET['id'])
      $album = $a;

if($_GET['fetch']) {
  include('template-album.php');
  exit;
}

$clsbody = $PAGE_STATES[$album ? 'album-single' : 'album-collection'];
$clsbody .= ' lock-box-position';
include('template.php');
