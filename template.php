<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title><?php echo $title ? $title .' - ' : ''; ?>Lai Yeg On</title>
    <link href="<?php echo BASE_URL; ?>/css/style.css?bc=<?php echo time(); ?>" rel="stylesheet" type="text/css"/>
    <script src="<?php echo BASE_URL; ?>/js/app.js"></script>
  </head>
  <body class="<?php echo $clsbody; ?>">
    <nav class="nav-main">
      <ul>
        <li><a href="<?php echo BASE_URL; ?>/album">Photos</a></li>
        <li><a href="<?php echo BASE_URL; ?>/story">Stories</a></li>
        <li><a href="<?php echo BASE_URL; ?>/about">About</a></li>
      </ul>
    </nav>
    <header>
      <strong>In Memory of</strong>
      <strong class="laiyegon">黎益安 / Lai Yeg On</strong>
      <strong class="life">July 25, 1935 to July 12, 2020</strong>
      <strong>Loving husband, father and grandfather</strong>
    </header>
    <section class="box album">
      <a href="<?php echo BASE_URL; ?>/album" class="box-label" data-page-state="album-collection">Albums</a>
      <div class="collection">
        <?php if(isset($albums)) : ?>
        <h1 style="background-image:url('<?php echo $albums[0]->thumbs[0]->src; ?>');"><span>Albums</span></h1>
        <ul class="gallery">
          <?php foreach($albums as $item) : ?>
          <li>
            <a href="<?php echo BASE_URL; ?>/album/<?php echo $item->href; ?>" data-page-state="album-single" data-fetch="1">
              <figure><img src="<?php echo $item->thumbs[0]->src; ?>" alt="<?php echo $item->title; ?>"/></figure>
              <figcaption><?php echo $item->title; ?></figcaption>
            </a>
          </li>
          <?php endforeach; ?>
        </ul>
        <?php endif; ?>
      </div>
      <div class="single"><?php include(BASE_PATH.'/template-album.php'); ?></div>
    </section>
    <section class="box story">
      <a href="<?php echo BASE_URL; ?>/story" class="box-label" data-page-state="story-collection">Stories</a>
      <div class="collection">
        <?php if(isset($stories)) : ?>
        <h1><span>Stories</span></h1>
        <ul>
          <?php foreach($stories as $item) : ?>
          <li>
            <h3><a href="<?php echo BASE_URL; ?>/story/<?php echo $item->href; ?>" title="<?php echo $item->title; ?>" data-page-state="story-single" data-fetch="1"><?php echo $item->title; ?></a></h3>
            <p class="publish-date"><?php echo date('F j, Y',strtotime($item->create_date)); ?></p>
            <div class="excerpt"><?php echo $item->excerpt; ?></div>
          </li>
          <?php endforeach; ?>
        </ul>
        <?php endif; ?>
      </div>
      <div class="single"><?php include(BASE_PATH.'/template-story.php'); ?></div>
    </section>
    <section class="box about">
      <a href="<?php echo BASE_URL; ?>/about" class="box-label" title="About Lai Yeg On" data-page-state="about">About</a>
      <div class="collection">
        <h1><span>Coming soon...</span></h1>
      </div>
    </section>
  </body>
</html>
