<?php
include('common.php');

$story = null;
if(isset($_GET['id']))
  foreach($stories as $s)
    if($s->href == $_GET['id'])
      $story = $s;

if($_GET['fetch']) {
  include('template-story.php');
  exit;
}

$clsbody = $PAGE_STATES[$story ? 'story-single' : 'story-collection'];
$clsbody .= ' lock-box-position';
include('template.php');
