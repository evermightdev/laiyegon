<?php if(!defined('BASE_PATH')) include_once('../../init.php'); ?>
<p class="author"><strong>by John Lai</strong></p>
<p><em>Published: November 25, 2019</em></p>

<p>My dad pointed out this recent picture of Up Down 9th Street (上下九步行街) in Guanzhou China. Dad said back in 1939, when he was 4 years old, he sat in the corner (I circled in red) every day begging for money and foat was HIS CORNER. Other homeless children and adults would take other parts of the street and storefronts. In 1939, the store behind my dad was a jewelry store. Dad thinks that the jewelry store owner had Parkinson's because his body was always trembling. The jewelry store owner would sometimes give food to my dad. Dad recalls a lot of Japanese soldiers patrolling the streets. One of the Japanese soldiers hit my dad with a rifle.</p>

<p><img style="width:100%;" src="/content/images/stomping-grounds.png" /></p>

