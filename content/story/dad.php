<?php if(!defined('BASE_PATH')) include_once('../../init.php'); ?>
<p class="author"><strong>by John Lai</strong></p>
<p><em>Published: June 30, 2021</em></p>
<p>My dad comes from an era of hardship.</p>
<p>He was born in China in 1935.
He was a child street begger.
He survived the Japanese occupation.
He survived World War 2.
He survived the Chinese Civil War.</p>

<p>With everyone around him starving and dying, my dad left his family at the age of 13, looking for work, looking for shelter and looking for food.  Every two or three years, my dad left for a different country when local authorities requested that he leave.  Dad jumped between China, Hong Kong, Peru, USA and Canada. Along the way, dad taught himself to read, write and speak Chinese, English and Spanish.</p>

<p>Dad eventually settled in Canada in the 1970s.  Dad said he broke down in tears when Canada offered him Canadian citizenship.  My dad said Canada is the best country in the world. He said he owed everything our family achieved to this great country.</p>

<p>Dad would marry my mom in late 1970s.  My brother and I would be born shortly after.  Our family would encountered many more challenges in the next 20 years, but nothing comparable to the problems of my dad's younger years.</p>

<p>Dad would finally be able to retire in his mid-70s. Dad was able to stay happy and free of anxiety for the first time in his life.  He watched my brother and I succeed in life.  My dad looked forward to being with his grand children every day.</p>

<p>In 2020, my dad was diagnosed with pancreatic cancer and would pass away six weeks later.  In his final days, my dad spoke again of how grateful he was to Canada for giving him a chance to have a family of is own.  My dad said 80 years ago, he thought he would perish like other children on the streets of a wartorn nation.  He couldn't even dream of living to the age of 80.  He couldn't even dream of living in a house. He couldn't even dream of having a full belly.  But he survived.  He fell in love with my mom.  And he started a great family.  Dad said it was Canada that made this happiness possible.</p>

<p>Thank you to all the health care workers and social workers that made the final days of my dad's life comfortable.</p>

