<?php if(!defined('BASE_PATH')) include_once('../../init.php'); ?>
<p class="author"><strong>by John Lai</strong></p>
<p><em>Published: June 20, 2021</em></p>

<p>I'm learning to play guitar.  Here's my take on a song my dad really liked.</p>
<iframe style="display:block; margin: 0 auto;" width="560" height="315" src="https://www.youtube.com/embed/uOOI1x4B7TA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

