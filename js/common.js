document.addEventListener('DOMContentLoaded',_=>{
  window.onpopstate = e =>{
    if(e.state) {
      const {pageState,fetch,href} = e.state;
      renderPage(pageState, fetch ? href : null);
    } else
      renderPage('home',null);
  };
  Array.from(document.querySelectorAll('[data-page-state]'))
    .forEach(item=>item.addEventListener('click',
      e=>{
        e.preventDefault();
        const href = item.getAttribute('href');
        const pageState = item.getAttribute('data-page-state');
        const fetch = item.getAttribute('data-fetch');
        renderPage(pageState, fetch ? href : null);
        history.pushState({pageState,href,fetch},'Lai Yeg On',href);
      })
    );
});

const renderPage = (pageState,url) => {
  if(typeof PAGE_STATES[pageState] === 'undefined')
    return;

  let prevState = 'home';
  if(document.body.classList.contains('body-story'))
    prevState = 'story';
  if(document.body.classList.contains('body-album'))
    prevState = 'album';
  if(document.body.classList.contains('body-about'))
    prevState = 'about';

  reservedStates().forEach(cls=>document.body.classList.remove(cls));

  PAGE_STATES[pageState]
    .replace(/\s+/,' ')
    .split(' ')
    .forEach(cls=>document.body.classList.add(cls));

	// animation hack because of the position absolute/fixed and top position animation
	// https://stackoverflow.com/questions/63781577/use-css-to-animate-a-box-to-fill-a-whole-screen-without-any-abrupt-jumping
	const scrollpos = window.pageYOffset || document.documentElement.scrollTop;
  if(pageState === 'home' && prevState !== 'home') {

		setTimeout(()=>{
		},T_SHOW_BOX_HALF);
		setTimeout(()=>{
			document.body.classList.remove('lock-box-position');
		},T_SHOW_BOX);

  } else {

		setTimeout(()=>{
			document.querySelector('.body-story .story,.body-album .album,.body-about .about').style.top = scrollpos+'px';
		},T_SHOW_BOX_HALF);
		setTimeout(()=>{
			document.querySelector('.body-story .story,.body-album .album,.body-about .about').style.removeProperty('top');
			document.body.classList.add('lock-box-position');
		},T_SHOW_BOX);
  }

  if(url) {
    fetch(url+'?fetch=1')
    .then(response=>response.text())
    .then(result=>{
      document.querySelector('.body-story .story .single,.body-album .album .single').innerHTML = result;
    })
    .catch(e=>console.log(e));
  }
}

const reservedStates = () =>
  Object.keys(PAGE_STATES)
    .map(k=>PAGE_STATES[k].replace(/\s+/,' ').split(' '))
    .reduce((carry,item)=>{item.forEach(i=>carry.push(i)); return carry;},[]);

