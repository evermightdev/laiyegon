<?php
include('../css/settings.php');
$entry = [];
foreach($PAGE_STATES as $prop=>$val)
  $entry[] = '"'.$prop.'":"'.$val.'"';
?>
const PAGE_STATES = {<?php echo implode(',',$entry); ?>};
const T_SHOW_BOX = <?php echo $t_show_box*1000; ?>;
const T_SHOW_BOX_HALF = <?php echo $t_show_box_half*1000; ?>;
