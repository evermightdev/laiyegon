<?php
header('Content-Type: text/css');
include('settings.php');

function b64($file) { echo base64_encode(file_get_contents($file)); }
include('reset.css');
include('fonts.css');
include('global.css');
include('box.css');
include('box-open.css');
include('header.css');
include('album.css');
include('story.css');
include('about.css');
