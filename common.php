<?php
include('init.php');

$content = json_decode(file_get_contents(BASE_PATH.'/content/album.json'));
$albums = $content;
$content = json_decode(file_get_contents(BASE_PATH.'/content/story.json'));
$stories = $content;

$PAGE_STATES = [
  'home'=>'body-home',
  'about'=>'body-about body-collection',
  'album-collection'=>'body-album body-collection',
  'album-single'=>'body-album body-single',
  'story-collection'=>'body-story body-collection',
  'story-single'=>'body-story body-single'
];
